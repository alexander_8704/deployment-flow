def componentNames = ['fluidwise', 'processes', 'userstatus', 'oauthserver', 'audit', 'flowable', 'saludtools_ui', 'tumedico_ui']
def approvalInput = null
def backendComponents = null
def frontEndComponents = null
def selectedComponents = []
def databaseScripts = []

pipeline {
  agent any
  options {
    skipDefaultCheckout true
  }
  tools {
    jdk 'JDK_1_8'
    maven 'Maven3_6_3'
  }
  environment {
    TARGET_ENV = "${env.BRANCH_NAME == 'master' ? 'prod' : (env.BRANCH_NAME == 'prerelease' ? 'qa' : 'dev')}"
    NOTIF_ENGINE_REPOSITORY_URL = credentials('notification-engine-repository-url')
    SAUDTOOLSV2_REPOSITORY_URL = credentials('saludtoolsv2-repository-url')
    USER_STATUS_REPOSITORY_URL = credentials('user-status-repository-url')
    OAUTH_SERVER_REPOSITORY_URL = credentials('oauth-server-repository-url')
    AUDIT_REPOSITORY_URL = credentials('audit-repository-url')
    FLOWABLE_REPOSITORY_URL = credentials('flowable-repository-url')
    SALDTOOLS_UI_REPOSITORY_URL = credentials('saludtools-ui-repository-url')
    TUMEDICO_UI_REPOSITORY_URL = credentials('tumedico-ui-repository-url')
  }
  stages {
    stage('Approval') {
      when {
        expression {
          TARGET_ENV == 'prodee'
        }
      }

      steps {
        emailext(
                subject: "JENKINS - Aprobar paso a ${TARGET_ENV.toUpperCase()}: '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                body: """<p>En este momento se encuentra en fase de aprobaci&oacute;n el paso a <span style='font-weight:bold'>${
                  TARGET_ENV.toUpperCase()
                }</span> de
                un nuevo release para el proyecto <span style='font-weight:bold'>${env.JOB_NAME}</span>. Haga click en el siguiente enlace para ir a la ejecuci&oacute;n
                y proceder con la autorizaci&oacute;n</p>
                <br/>
                <a href='${env.RUN_DISPLAY_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>""",
                to: '$DEFAULT_RECIPIENTS'
        )
        script {
          timeout(time: 15, unit: 'MINUTES') {
            approvalInput = input(
                    id: 'approvalInput',
                    message: 'Ingrese el c\u00f3digo de aprobaci\u00f3n de esta versi\u00f3n',
                    ok: 'Aprobar',
                    submitterParameter: 'APPROVAL_KEY_SUBMITTER',
                    parameters: [
                            password(name: 'APPROVAL_KEY', description: 'C\u00f3digo de aprobaci\u00f3n de la versi\u00f3n', defaultValue: '')
                    ])
          }
          withCredentials([usernamePassword(credentialsId: 'jenkins-production-approval-key', usernameVariable: 'expectedApprovalAccount', passwordVariable: 'expectedApprovalKey')]) {
            if (expectedApprovalAccount.contains(approvalInput.APPROVAL_KEY_SUBMITTER) && expectedApprovalKey.equals(approvalInput.APPROVAL_KEY.toString())) {
              echo "${approvalInput.APPROVAL_KEY_SUBMITTER} ha aprobado el paso a ${TARGET_ENV}"
            } else if (!expectedApprovalAccount.contains(approvalInput.APPROVAL_KEY_SUBMITTER)) {
              currentBuild.result = 'ABORTED'
              error("Ejecuci\u00f3n abortada, usted no est\u00e1 autorizado para aprobar despliegues a ${TARGET_ENV.toUpperCase()}")
            } else {
              currentBuild.result = 'ABORTED'
              error('Ejecuci\u00f3n abortada, el c\u00f3digo de aprobaci\u00f3n ingresado no es valido')
            }
          }
        }
      }
    }

    stage('Component Selection') {
      steps {
        script {
          timeout(time: 5, unit: 'MINUTES') {
            backendComponents = input(
              message: 'Seleccione los componentes de Back-end a desplegar',
              id: 'backendComponents',
              ok: 'Continuar',
              parameters: [
                booleanParam(defaultValue: false, description: 'Microservicio fluidwise', name: 'fluidwise'),
                booleanParam(defaultValue: false, description: 'Microservicio Massive Processes', name: 'processes'),
                booleanParam(defaultValue: false, description: 'Microservicio User Status Service', name: 'userstatus'),
                booleanParam(defaultValue: false, description: 'Microservicio OAuth 2.0', name: 'oauthserver'),
                booleanParam(defaultValue: false, description: 'Microservicio Auditoria de Login', name: 'audit'),
                booleanParam(defaultValue: false, description: 'Microservicio BPM (Flowable)', name: 'flowable')
              ]
            )
          }
          timeout(time: 5, unit: 'MINUTES') {
            frontEndComponents = input(
                    message: 'Seleccione los componentes de Front-end a desplegar',
                    id: 'frontEndComponents',
                    ok: 'Continuar',
                    parameters: [
                            booleanParam(defaultValue: false, description: 'UI Saludtools', name: 'saludtools_ui'),
                            booleanParam(defaultValue: false, description: 'UI TuMedico', name: 'tumedico_ui')
                    ]
            )
          }
          selectedComponents = [
                  componentNames.findAll{backendComponents[it]},
                  componentNames.findAll{frontEndComponents[it]},
          ].flatten()

          if(selectedComponents.isEmpty()) {
            currentBuild.result = 'ABORTED'
            error("Ejecuci\u00f3n abortada, debe seleccionar al menos un componente de Back-end o Front-end a desplegar")
          } else {
            echo "Se desplegar\u00e1n los siguientes componentes:\n ${selectedComponents.collect { '\t- ' + it }.join('\n')}"
          }
        }
      }
    }

    /** -----------------------------------------------------------------------------------------
     * Fluidwise And Massive Processes Build
     -----------------------------------------------------------------------------------------  */
    stage('Build fluidwise and/or massive-processes') {
      when {
        expression {
          backendComponents.fluidwise || backendComponents.processes
        }
      }
      steps {
        dir('carecloud-notification-engine') {
          git(url: "${NOTIF_ENGINE_REPOSITORY_URL}", branch: "${env.BRANCH_NAME}", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          dir('carecloud-notification-engine') {
            sh "mvn clean install"
          }
        }
        dir('saludtoolsv2') {
          //:TODO: Replace static branchName by ${env.BRANCH_NAME}
          git(url: "${SAUDTOOLSV2_REPOSITORY_URL}", branch: "master2", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          dir('CoreCareCloud') {
            sh "mvn clean install"
          }
          dir('saludtools') {
            sh "mvn clean install"
          }
        }
        sh """
            mkdir -p fluidwise && mkdir -p processes
            mv -f saludtoolsv2/saludtools/fluidwise/target/*.war fluidwise/app.jar
            mv -f saludtoolsv2/saludtools/massive-processes/target/*.war processes/app.jar
        """
      }
    }

    /** -----------------------------------------------------------------------------------------
     * Build User status
     -----------------------------------------------------------------------------------------  */
    stage('Build userstatus') {
      when {
        expression {
          backendComponents.userstatus
        }
      }

      steps {
        dir('carecloud-user-status-service') {
          git(url: "${USER_STATUS_REPOSITORY_URL}", branch: "${env.BRANCH_NAME}", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          dir('carecloud-user-status-service') {
            sh "mvn clean install"
          }
        }
        sh """
            mkdir -p userstatus
            mv -f carecloud-user-status-service/carecloud-user-status-service/target/*.jar userstatus/app.jar
        """
      }
    }

    /** -----------------------------------------------------------------------------------------
     * Build OAuth Server
     -----------------------------------------------------------------------------------------  */
    stage('Build OAuth Server') {
      when {
        expression {
          backendComponents.oauthserver
        }
      }

      steps {
        dir('carecloud-security-backend') {
          git(url: "${OAUTH_SERVER_REPOSITORY_URL}", branch: "${env.BRANCH_NAME}", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          dir('Security') {
            sh "mvn clean install"
          }
        }
        sh """
            mkdir -p oauthserver
            mv -f carecloud-security-backend/Security/target/*.jar oauthserver/app.jar
        """
      }
    }

    /** -----------------------------------------------------------------------------------------
     * Build Audit
     -----------------------------------------------------------------------------------------  */
    stage('Build audit') {
      when {
        expression {
          backendComponents.audit
        }
      }

      steps {
        dir('carecloud-audit') {
          git(url: "${AUDIT_REPOSITORY_URL}", branch: "${env.BRANCH_NAME}", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          dir('CarecloudAudit') {
            sh "mvn clean install"
          }
        }
        sh """
            mkdir -p audit
            mv -f carecloud-audit/CarecloudAudit/target/*.jar audit/app.jar
        """
      }
    }

    /** -----------------------------------------------------------------------------------------
     * Build BPM
     -----------------------------------------------------------------------------------------  */
    stage('Build BPM (Flowable)') {
      when {
        expression {
          backendComponents.flowable
        }
      }

      steps {
        dir('saludtools-bpm-engine') {
          git(url: "${FLOWABLE_REPOSITORY_URL}", branch: "${env.BRANCH_NAME}", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          dir('saludtools-bpm-engine') {
            sh "mvn clean install"
          }
        }
        sh """
            mkdir -p flowable
            mv -f saludtools-bpm-engine/saludtools-bpm-engine/target/*.war flowable/app.jar
        """
      }
    }

    /** -----------------------------------------------------------------------------------------
     * Build Saludtools UI
     -----------------------------------------------------------------------------------------  */
    stage('Build Saludtools UI') {
      when {
        expression {
          frontEndComponents.saludtools_ui
        }
      }

      steps {
        dir('frontendsaludtoolsv2') {
          git(url: "${SALDTOOLS_UI_REPOSITORY_URL}", branch: "${env.BRANCH_NAME}", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          sh """
              grunt --version || npm install -g grunt-cli
              npm install
              npx grunt build --buildNum=${env.BUILD_ID} --env=${TARGET_ENV}
          """
        }
        sh """
            mkdir -p saludtools_ui
            tar -zcf saludtools_ui/dist.tar.gz -C frontendsaludtoolsv2/build/ .
        """
      }
    }

    stage('Build TuMedico UI') {
      when {
        expression {
          frontEndComponents.tumedico_ui
        }
      }

      steps {
        dir('tumedico-web-ui') {
          git(url: "${TUMEDICO_UI_REPOSITORY_URL}", branch: "${env.BRANCH_NAME}", credentialsId: 'jenkinsmaster-bitbucket-sshkey')
          sh """
              gulp --version || npm install -g gulp-cli
              npm install
              npx gulp ${TARGET_ENV}
          """
        }
        sh """
            mkdir -p tumedico_ui
            tar -zcf tumedico_ui/dist.tar.gz -C tumedico-web-ui/dist/ .
        """
      }
    }

  }

  post {
    always {
      script {
        deleteDir()
      }
    }
  }

}